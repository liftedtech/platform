# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161201025133) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string   "address"
    t.float    "lat"
    t.float    "lng"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "alexa_top_sites", force: :cascade do |t|
    t.integer  "rank"
    t.string   "domain_name"
    t.date     "date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "comments", force: :cascade do |t|
    t.string   "comment_body"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "comments", ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id", using: :btree

  create_table "google_business_malwares", force: :cascade do |t|
    t.string   "url"
    t.integer  "place_id"
    t.jsonb    "json_response"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "campaign_id"
  end

  create_table "keywords", force: :cascade do |t|
    t.string   "key"
    t.string   "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "leads", force: :cascade do |t|
    t.string   "url"
    t.string   "formatted_address"
    t.string   "phone_number"
    t.integer  "vulnerability_count"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "place_details", force: :cascade do |t|
    t.integer  "place_id"
    t.string   "name"
    t.string   "google_id"
    t.string   "google_place_id"
    t.string   "url"
    t.string   "utc_offset"
    t.string   "vicinity"
    t.string   "website"
    t.string   "international_phone_number"
    t.text     "raw"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "city"
    t.string   "neighborhood"
    t.string   "county"
    t.string   "country"
    t.string   "postal_code"
    t.string   "state"
    t.string   "street_no"
    t.integer  "rating"
    t.boolean  "fetched_semrush_response",   default: false
    t.string   "tag"
    t.string   "formatted_address"
    t.string   "lat"
    t.string   "lng"
  end

  create_table "places", force: :cascade do |t|
    t.string   "place_id"
    t.float    "lat"
    t.float    "lng"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "keyword_id"
  end

  create_table "semrush_paid_reports", force: :cascade do |t|
    t.string   "url"
    t.text     "response"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "campaign_id"
    t.string   "tag"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "whatwebs", force: :cascade do |t|
    t.string   "url"
    t.text     "response"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "tag"
    t.string   "campaign_id"
  end

  create_table "wpscans", force: :cascade do |t|
    t.string   "url"
    t.text     "response"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "sidekiqID"
    t.string   "cid"
    t.string   "status"
    t.string   "campaign"
    t.integer  "vulnerability_count"
    t.boolean  "claimed",             default: false
    t.string   "tag"
  end

end
