class AddStreetAndRatingToPlaceDetails < ActiveRecord::Migration
  def change
    add_column :place_details, :street_no, :string
    add_column :place_details, :rating, :integer
  end
end
