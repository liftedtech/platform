class AddCampaignToSemrushPaidReport < ActiveRecord::Migration
  def change
    add_column :semrush_paid_reports, :campaign_id, :string
  end
end
