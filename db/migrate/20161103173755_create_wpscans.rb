class CreateWpscans < ActiveRecord::Migration
  def change
    create_table :wpscans do |t|
      t.string :url
      t.jsonb :response

      t.timestamps null: false
    end
  end
end
