class ChangeWpScanToText < ActiveRecord::Migration
  def up
      change_column :wpscans, :response, :text
  end
  def down
      # This might cause trouble if you have strings longer
      # than 255 characters.
      change_column :wpscans, :response, :jsonb
  end
end
