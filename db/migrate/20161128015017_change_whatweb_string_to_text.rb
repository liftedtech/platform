class ChangeWhatwebStringToText < ActiveRecord::Migration
def up
    change_column :whatwebs, :response, :text
end
def down
    # This might cause trouble if you have strings longer
    # than 255 characters.
    change_column :whatwebs, :response, :string
end
end
