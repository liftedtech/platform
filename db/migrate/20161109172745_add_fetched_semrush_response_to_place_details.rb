class AddFetchedSemrushResponseToPlaceDetails < ActiveRecord::Migration
  def change
    add_column :place_details, :fetched_semrush_response, :boolean,default: false
  end
end
