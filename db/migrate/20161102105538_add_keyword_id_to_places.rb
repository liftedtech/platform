class AddKeywordIdToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :keyword_id, :integer
  end
end
