class AddFormattedAddressToPlaceDetails < ActiveRecord::Migration
  def change
    add_column :place_details, :formatted_address, :string
  end
end
