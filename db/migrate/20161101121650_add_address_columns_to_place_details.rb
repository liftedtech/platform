class AddAddressColumnsToPlaceDetails < ActiveRecord::Migration
  def change
    add_column :place_details, :city, :string
    add_column :place_details, :neighborhood, :string
    add_column :place_details, :county, :string
    add_column :place_details, :country, :string
    add_column :place_details, :postal_code, :string
    add_column :place_details, :state, :string
  end
end
