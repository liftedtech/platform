class AddSidekiqIDandCampaignIDandStatusToWpscan < ActiveRecord::Migration
  def change
    add_column :wpscans, :sidekiqID, :string
    add_column :wpscans, :cid, :string
    add_column :wpscans, :status, :string
  end
end
