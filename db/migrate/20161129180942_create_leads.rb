class CreateLeads < ActiveRecord::Migration
  def change
    create_table :leads do |t|
      t.string :url
      t.string :formatted_address
      t.string :phone_number
      t.integer :vulnerability_count

      t.timestamps null: false
    end
  end
end
