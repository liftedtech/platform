class AddLatLngToPlaceDetails < ActiveRecord::Migration
  def change
    add_column :place_details, :lat, :string
    add_column :place_details, :lng, :string
  end
end
