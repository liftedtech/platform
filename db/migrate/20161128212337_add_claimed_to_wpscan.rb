class AddClaimedToWpscan < ActiveRecord::Migration
  def change
    add_column :wpscans, :claimed, :boolean, default: false
  end
end
