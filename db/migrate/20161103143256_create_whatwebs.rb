class CreateWhatwebs < ActiveRecord::Migration
  def change
    create_table :whatwebs do |t|
      t.string :url
      t.jsonb :response

      t.timestamps null: false
    end
  end
end
