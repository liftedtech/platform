class CreateSemrushPaidReports < ActiveRecord::Migration
  def change
    create_table :semrush_paid_reports do |t|
      t.string :url
      t.text :response

      t.timestamps null: false
    end
  end
end
