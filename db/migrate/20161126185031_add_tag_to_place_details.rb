class AddTagToPlaceDetails < ActiveRecord::Migration
  def change
    add_column :place_details, :tag, :string
  end
end
