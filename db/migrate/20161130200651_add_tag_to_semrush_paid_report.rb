class AddTagToSemrushPaidReport < ActiveRecord::Migration
  def change
    add_column :semrush_paid_reports, :tag, :string
  end
end
