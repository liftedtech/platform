class CreatePlaceDetails < ActiveRecord::Migration
  def change
    create_table :place_details do |t|
      t.references :place
      t.string :name
      t.string :google_id
      t.string :google_place_id
      t.string :url
      t.string :utc_offset
      t.string :vicinity
      t.string :website
      t.string :international_phone_number
      t.text :raw

      t.timestamps null: false
    end
  end
end
