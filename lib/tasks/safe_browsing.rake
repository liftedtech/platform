namespace :scrape_data do
  desc 'safe browsing api'
  task :safe_browsing_api => :environment do
    key = GOOGLE_SAFEBROWSING_KEY
    url = "https://safebrowsing.googleapis.com/v4/threatMatches:find?key=#{key}"
    PlaceDetail.select(:url,:place_id).each do |place|
      begin
        canonical_url = canonical(place.url)
        response = HTTParty.post(url,
          :body => {:client=>{:clientId=>'opslog', :clientVersion=>'1.0.0'},
            :threatInfo=>{:platformTypes=>['ANY_PLATFORM'],
              :threatEntries=>[{:url=>canonical_url}],
              :threatEntryTypes=>['THREAT_ENTRY_TYPE_UNSPECIFIED',
                'URL',
                'EXECUTABLE',
                'IP_RANGE'],
              :threatTypes=>['THREAT_TYPE_UNSPECIFIED',
                'MALWARE',
                'SOCIAL_ENGINEERING',
                'UNWANTED_SOFTWARE',
                'POTENTIALLY_HARMFUL_APPLICATION']}}.to_json,
          :headers => { 'Content-Type' => 'application/json' } )
        if response.empty?
          puts "No malware detected for #{place.url}"
        else
          if response.code == 200
            GoogleBusinessMalware.create(url: place.url,place_id: place.place_id,json_response: response.body)
            puts "Malware detected for #{place.url}"
          end
        end
      rescue => e
        puts '[>.<] ERROR: ', e.message
    end
    end
  end
end

def canonical(url='')
  url.sub(/^https?\:\/\/(www.)?/,'')
end
