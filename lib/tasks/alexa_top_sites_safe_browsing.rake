namespace :scrape_data do
  desc 'safe browsing api'
  task :alexa_sites_safe_browsing => :environment do
    (1..2000).each do |index|
      AlexaTopSitesSafeBrowsingWorker.new.perform(index)
    end
  end
end
