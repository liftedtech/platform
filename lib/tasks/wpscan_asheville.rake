namespace :wpscan do
  desc 'run shells scripts on google server'
  task :asheville => :environment do

    place_details = Whatweb.where('response LIKE ?', '%WordPress%')
    place_details.each do |wp|
      begin
        WpscanWorker.perform_async(wp.url)
      rescue => e
        puts '[>.<] ERROR: ', e.message
      end

    end

    place_details = Joomscan.where('response LIKE ?', '%Joomla%')
    place_details.each do |wp|
      begin
        JoomscanWorker.perform_async(wp)
      rescue => e
        puts '[>.<] ERROR: ', e.message
      end

    end


  end
end
