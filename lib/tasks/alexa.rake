require 'net/http'
require 'zip'
require 'csv'  

namespace :alexa do
	desc 'download zip file of Alexa site'
  task download_site_file: :environment do
		puts "Loading tasks..."
		path = "http://s3.amazonaws.com/alexa-static/top-1m.csv.zip"
		uri = URI(path)
    File.open('top-sites.csv.zip',"wb") do |file|
			file.write Net::HTTP.get(uri)
		end
  end

	desc 'download zip file of Alexa site'
  task import_sites: :environment do
    if File.exists? 'top-sites.csv.zip'
			Zip::ZipFile.foreach('top-sites.csv.zip') do |entry|
        istream = entry.get_input_stream
				data = istream.read
				csv = CSV.parse(data)
				csv.each do |row|
					begin
						unless AlexaTopSite.find_by(domain: row[1]).present?
							a = AlexaTopSite.create(rank: row[0].to_i, domain: row[1], date: Date.current)
							p a.domain
						end
					rescue
						nil
					end
				end
				File.delete('top-sites.csv.zip')
			end
		end
	end


  desc "fetch address"
  task top_site: :environment do
  	#Download the Zipped File
	  puts "Loading tasks..."
	  path = "http://s3.amazonaws.com/alexa-static/top-1m.csv.zip"
	  uri = URI(path)
	  zipped_folder = Net::HTTP.get(uri)
	  #Unzip the downloaded file
	    unzipped_file = Zip::File.open(zipped_folder) do |zip_file|
	        zip_file.each do |f|
	            f_path = File.join("#{Rails.root}", f.name)
	            FileUtils.mkdir_p(File.dirname(f_path))
	            f.extract(f_path) 
	        end
	    end
	    #Importing Data
	    csv_text = File.read("#{Rails.root}/public/#{unzipped_file.first[1].name}")
	    csv = CSV.parse(csv_text, :headers => true)
	    csv.each do |row|
	        begin
	            unless AlexaTopSite.find_by(domain: row[1]).present?
	            a = AlexaTopSite.create(rank: row[0].to_i, domain: row[1], date: Date.current)
	            p a.domain
	          end
	      rescue
	        nil
	        end
	    end
	    File.delete("#{Rails.root}/public/#{unzipped_file.first[1].name}")
  end
end
