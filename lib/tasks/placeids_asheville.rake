namespace :placeids do

  require 'benchmark'
                                                                                                                                       namespace :wpscan_asheville do
  desc 'scrape place_ids into Place using huge kw list against asheville nc'

  task :asheville => :environment do

    keywords = []
    file_name="keywords_big.txt"
    path = Rails.root.join('lib/tasks', file_name)
    f = File.open(path)
    records = f.readlines
    records.each do |record|
      keywords << record
    end

    google_key = 'AIzaSyADZOp2jbnLd0UTXERo5xDMeLJp45XKLk4'
    latitude="35.5951" #asheville, nc
    longitude="-82.5515" #asheville, nc

    print "running kws"
    keywords.each do |keyword|
        keyword = keyword.strip
        url = 'https://maps.googleapis.com/maps/api/place/radarsearch/json?key=' + google_key + '&location=' + latitude + ',' + longitude + '&keyword=' + keyword + '&radius=100000'
        print "[!] Querying =#{keyword} via #{url}"
        results = {}
        uri = URI.parse(url)
        http = Net::HTTP.new(uri.host, uri.port)
        http.open_timeout = 30
        http.read_timeout = 30
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        response = http.request_post("#{uri.path}?#{uri.query}", '')
        if Net::HTTPOK # 200
          results = response.body
          json = JSON.parse(results)
          print "--- start of json response ---"
          print json.inspect
          print "--- end of json response ---"
        end
      end
    end
  end
end
