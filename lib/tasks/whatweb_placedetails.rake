namespace :whatweb do

  require 'benchmark'
                                                                                                                                       namespace :wpscan_asheville do
  desc 'run wpscans on worker machines'

  task :placedetails => :environment do
    place_details = PlaceDetail.all #:website)
    place_details.each do |place_detail|
      WhatwebWorker.perform_async(place_detail.website) # queue up all places from SemrushPaidReport (Asheville leads)
    end
  end
end
end
