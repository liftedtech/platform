require 'csv'
namespace :semrush do
  desc 'generate csv file.'
  task :csv_report => :environment do
    semrush_paid_reports = SemrushPaidReport.all
    header = "name,website,phone,address,whatweb_response,semrush_response"
    file = "my_file.csv"
    File.open(file, "w") do |csv|
      csv << header
      semrush_paid_reports.each do |semrush_paid_report|
        place_detail = PlaceDetail.where(website: semrush_paid_report.url).first
        whatweb = Whatweb.find_by_url semrush_paid_report.url
        csv << [place_detail.name, place_detail.website, place_detail.international_phone_number, place_detail.vicinity,whatweb.response,semrush_paid_report.response]       
      end
    end
  end
end