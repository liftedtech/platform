require 'csv'
namespace :generate_csv do
  desc 'generate csv file.'
  task :asheville_csv => :environment do
    wpscans = Wpscan.where('response["result"] like ?', '%identified from version%')
    header = "id^^^url^^^response"
    file = "my_file.csv"
    File.open(file, "w") do |csv|
      wpscans.each do |wpscan|
        csv << wpscan.id.to_s + "^^^" + wpscan.url + "^^^" + wpscan.response.to_s + "\n" rescue nil
      end
    end
  end
end
