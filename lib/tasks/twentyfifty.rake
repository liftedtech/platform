namespace :twentyfifty do                                                                                                                                   namespace :wpscan_asheville do
  desc '20x50'
  task :go => :environment do

    key = "AIzaSyADZOp2jbnLd0UTXERo5xDMeLJp45XKLk4"

    File.readlines('lib/tasks/address.txt').each do |add|
      url = 'https://maps.googleapis.com/maps/api/geocode/json' + '?key=' + key + '&address=' + add

      puts "[INFO]".colorize(:yellow) + "Geocoding #{add} via #{url}"

      response = HTTParty.get(url)
      json = response.parsed_response #    json = JSON.parse(results)

      lat = json['results'][0]['geometry']['location']['lat']
      lng = json['results'][0]['geometry']['location']['lng']

      File.readlines('lib/tasks/keywords1.txt').each do |businesstype|
        url = "https://maps.googleapis.com/maps/api/place/radarsearch/json?key=#{key}&location=#{lat},#{lng}&keyword=#{businesstype}&radius=50000"
        response = HTTParty.get(url)
        json = response.parsed_response

        json['results'].each do |place|
          print "[SUCCESS] Captured #{place['place_id']} from category(#{businesstype}). Grabbing details now..."
          GooglePlacesGetDetailsService.new.placesdetails(place['place_id'])
        end

      end
    end
  end
end
end
