require 'uri'
require 'net/https'

namespace :extractor do
  desc 'fetch address'
  task pull_address: :environment do
    googl_api_key = 'AIzaSyAdd2VqDHX6rMLWekvV4Nlu5OBomHiS4HA'
    path = Rails.root.join('lib/tasks', 'address.txt')
    f = File.open(path)
    records = f.readlines
    records.each do |add|
      url = 'https://maps.googleapis.com/maps/api/geocode/json' + '?key=' + googl_api_key + '&address=' + add
      puts url
      results = {}
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.open_timeout = 30
      http.read_timeout = 30
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      response = http.request_post("#{uri.path}?#{uri.query}", '')
      next unless Net::HTTPOK # 200
      results = response.body
      json = JSON.parse(results)
      json['results'].each do |r|
        if r['geometry']['bounds'].present? && !Address.where(address: add, lat: r['geometry']['bounds']['northeast']['lat'], lng: r['geometry']['bounds']['northeast']['lng']).present?
          Address.create!(address: add, lat: r['geometry']['bounds']['northeast']['lat'], lng: r['geometry']['bounds']['northeast']['lng'])
          puts 'added record'
        end

        if r['geometry']['bounds'].present? && !Address.where(address: add, lat: r['geometry']['bounds']['southwest']['lat'], lng: r['geometry']['bounds']['southwest']['lng']).present?
          Address.create!(address: add, lat: r['geometry']['bounds']['southwest']['lat'], lng: r['geometry']['bounds']['southwest']['lng'])
          puts 'added record'
        end

        unless Address.where(address: add, lat: r['geometry']['location']['lat'], lng: r['geometry']['location']['lng']).present?
          Address.create!(address: add, lat: r['geometry']['location']['lat'], lng: r['geometry']['location']['lng'])
          puts 'added record'
        end

        unless Address.where(address: add, lat: r['geometry']['viewport']['northeast']['lat'], lng: r['geometry']['viewport']['northeast']['lng']).present?
          Address.create!(address: add, lat: r['geometry']['viewport']['northeast']['lat'], lng: r['geometry']['viewport']['northeast']['lng'])
          puts 'added record'
        end

        unless Address.create!(address: add, lat: r['geometry']['viewport']['southwest']['lat'], lng: r['geometry']['viewport']['southwest']['lng']).present?
          Address.create!(address: add, lat: r['geometry']['viewport']['southwest']['lat'], lng: r['geometry']['viewport']['southwest']['lng'])
          puts 'added record'
        end
      end
    end
  end

  desc 'fetch places'
  task pull_places: :environment do

    getch_places('keywords1.txt')
    getch_places('keywords2.txt')
  end

  desc 'fetch place details'
  task place_details: :environment do
    puts 'pulling places'
    google_key = 'AIzaSyADZOp2jbnLd0UTXERo5xDMeLJp45XKLk4'
    Place.all.each do |place|
      next if place.place_details.present?
      url = 'https://maps.googleapis.com/maps/api/place/details/json?key=' + google_key + '&placeid=' + place.place_id
      puts url
      results = {}
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.open_timeout = 30
      http.read_timeout = 30
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      response = http.request_post("#{uri.path}?#{uri.query}", '')
      next unless Net::HTTPOK # 200
      results = response.body
      json = JSON.parse(results)
      if json['status'] == 'OK'
        place.place_details.create(name: json['result']['name'], google_id: json['result']['id'], google_place_id: json['result']['place_id'], url: json['result']['url'], utc_offset: json['result']['utc_offset'], vicinity: json['result']['vicinity'], website: json['result']['website'], international_phone_number: json['result']['internation_phone_number'], raw: json['result'])
      end
    end
  end
end

def getch_places(file_name)
  keywords = []
  path = Rails.root.join('lib/tasks', file_name)
  f = File.open(path)
  records = f.readlines
  puts 'pulling keywords'
  records.each do |record|
    keywords << record
  end
  puts keywords

  puts 'pulling places'
  google_key = 'AIzaSyADZOp2jbnLd0UTXERo5xDMeLJp45XKLk4'
  address = nil
  keywords.each do |keyword|
    Address.all.each do |address|
      puts address.inspect
      puts keyword
      next unless !address.lat.nil? && !address.lat.nil? && !keyword.nil?
      keyword = keyword.strip
      url = 'https://maps.googleapis.com/maps/api/place/radarsearch/json?key=' + google_key + '&location=' + address.lat.to_s + ',' + address.lng.to_s + '&keyword=' + keyword + '&radius=50000'
      puts url
      results = {}
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.open_timeout = 30
      http.read_timeout = 30
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      response = http.request_post("#{uri.path}?#{uri.query}", '')
      if Net::HTTPOK # 200
        results = response.body
        json = JSON.parse(results)

        if json['status'] == 'OK'

          street_no = nil
          street_hash = json['result']['address_components'].find {|h| h['types'][0] == 'street_number'}
          street_no = street_hash['long_name'] unless street_hash.nil?

          city = json['result']['address_components'].find {|h| h['types'][0] == 'locality'}['long_name'] unless json['result']['address_components'].find {|h| h['types'][0] == 'locality'}.nil?
          neighborhood = json['result']['address_components'].find {|h| h['types'][0] == 'neighborhood'}['long_name'] unless json['result']['address_components'].find {|h| h['types'][0] == 'neighborhood'}.nil?
          county = json['result']['address_components'].find {|h| h['types'][0] == 'administrative_area_level_2'}['long_name'] unless json['result']['address_components'].find {|h| h['types'][0] == 'administrative_area_level_2'}.nil?
          state = json['result']['address_components'].find {|h| h['types'][0] == 'administrative_area_level_1'}['long_name'] unless json['result']['address_components'].find {|h| h['types'][0] == 'administrative_area_level_1'}.nil?
          country = json['result']['address_components'].find {|h| h['types'][0] == 'country'}['long_name'] unless json['result']['address_components'].find {|h| h['types'][0] == 'country'}.nil?
          postal_code = json['result']['address_components'].find {|h| h['types'][0] == 'postal_code'}['long_name'] unless json['result']['address_components'].find {|h| h['types'][0] == 'postal_code'}.nil?
          reviews = json['result']['reviews']
          rating = json['result']['reviews'].first['aspects'].find {|h| h['rating']}['rating'] unless reviews.nil?

          place.place_details.create(name: json['result']['name'],google_id: json['result']['id'], google_place_id: json['result']['place_id'],url: json['result']['url'],
                                     utc_offset: json['result']['utc_offset'], vicinity: json['result']['vicinity'],
                                     street_no: street_no, city: city, neighborhood: neighborhood,county: county,state: state,
                                     country: country, postal_code: postal_code, rating: rating || 0,
                                     website: json['result']['website'], international_phone_number:json['result']['internation_phone_number'] , raw: json['result'])
        end
      end
      puts '********************************************'
    end
  end
end
