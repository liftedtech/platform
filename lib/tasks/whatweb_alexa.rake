namespace :whatweb do

  require 'benchmark'
                                                                                                                                       namespace :wpscan_asheville do
  desc 'run wpscans on worker machines'

  task :alexa => :environment do
    place_details = AlexaTopSite.all #:website)
    AlexaTopSite.find_each do |place_detail|
      WhatwebWorker.perform_async(place_detail.domain_name, "alexa") # queue up all places from SemrushPaidReport (Asheville leads)
    end
  end
end
end
