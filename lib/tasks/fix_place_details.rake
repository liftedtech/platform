namespace :fix do
  desc 'fetch address'
  task place_details: :environment do


    PlaceDetail.find_each do |placedetail|
      begin
        json = JSON.parse(placedetail.raw.gsub("=>", ":")) rescue nil
        placedetail.website = json['website']
        placedetail.lat = json['geometry']['location']['lat']
        placedetail.lng = json['geometry']['location']['lng']
        placedetail.save
      rescue => e
        print e
      end
    end

  end
end
