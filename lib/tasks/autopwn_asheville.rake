namespace :autopwn do

  require 'benchmark'
                                                                                                                                       namespace :wpscan_asheville do
  desc 'identify local business website vulnerabilities in a chosen location'

  task asheville: :environment do

    # 1 - convert Asheville, NC to GPS coordinates
    ashevilleCoordinates = Address2gpsService.new.convert("Asheville, NC")

    lat = ashevilleCoordinates[lat]
    lng = ashevilleCoordinates[lng]

    # 2 - take our GPS coordinates and pass them to radarsearch to find all place_ids

    GooglePlacesRadarSearchService.new.radarsearch(lat,lng) #

    # 4 - perform whatweb scan for all Websites in all place_details

#   place_details.each do |place|
#      Whatweb.perform_async(place)
#    end

    # 5 - queue up WpscanService if 'WordPress' is within the response string whatweb shows

#    wordpressSites = Whatweb.where(response: contains 'WordPress') AND .where(cid: currentCampaignID)
#    wordpressSites.each do |site|
#      Wpscan.perform_async(site) # queue
#    end

    # 6 - sidekiq queue JoomscanService if 'Joomla' is within whatweb response string

#    joomlaSites = Whatweb.where(response: contains 'WordPress') AND .where(cid: currentCampaignID)
#    wordpressSites.each do |site|
#      Wpscan.perform_async(site) # queue
#    end

    # 7 - push all wordpressSites that have a Wpscan response containing ""vulnerabilities identified from the version number""

#    wordpressSites.each do |site|
#      if site.wpscanResponse.contains("vulnerabilities identified from the version number")
#        PushToSugarCRM(businessName, website, phone, address, category, wpscanReport) # we need to pass NAME,
#      end
#    end
  end
end
end
