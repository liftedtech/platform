namespace :semrush do
  desc 'run shells scripts on google server'
  task :paid_report => :environment do
    #    key = "1e8bb9ef2387e59b133"
    #    url = 'http://104.196.203.75:3000'
    #    search = PlaceDetail.where(fetched_semrush_response: false).search(name_cont: 'Asheville')
    #    place_details = search.result.select("DISTINCT(WEBSITE)")
    #    place_details.each do |place_detail|
    #      response = HTTParty.get("#{url}/v1/semrush_paid_reports?url=#{place_detail.website}&key=#{key}")
    #      puts '*************************', response
    #      semrush_paid_report = SemrushPaidReport.new(url: place_detail.website,response: response)
    #      if semrush_paid_report.save
    #        place_detail.update_attribute(fetched_semrush_response: true)
    #      end
    #    end
    semrush_responses(getch_places('keywords1.txt')+getch_places('keywords2.txt'))
  end
end

def getch_places(file_name)
  websites = []
  keywords = []
  path = Rails.root.join('lib/tasks', file_name)
  f = File.open(path)
  records = f.readlines
  puts 'pulling keywords'
  records.each do |record|
    keywords << record
  end
  puts keywords

  puts 'pulling places'
  google_key = 'AIzaSyADZOp2jbnLd0UTXERo5xDMeLJp45XKLk4'
  search = Address.search(address_cont: 'Asheville') 
  addresses = search.result
  address = addresses.first
  keywords.each do |keyword|
    puts address.inspect
    puts keyword
    keyword = keyword.strip
    url = 'https://maps.googleapis.com/maps/api/place/radarsearch/json?key=' + google_key + '&location=' + address.lat.to_s + ',' + address.lng.to_s + '&keyword=' + keyword + '&radius=50000'
    puts url
    results = {}
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.open_timeout = 30
    http.read_timeout = 30
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    response = http.request_post("#{uri.path}?#{uri.query}", '')
    if Net::HTTPOK # 200
      results = response.body
      json = JSON.parse(results)
      if json['status'] == 'OK'
        place_ids = json['results'].collect{|r| r['place_id']} 
        place_ids.each do |place_id|
          websites << get_place_id_details(place_id)
        end
      end
    end
    puts '********************************************'
  end
  return websites
end


def get_place_id_details(place_id)
  google_key = 'AIzaSyADZOp2jbnLd0UTXERo5xDMeLJp45XKLk4'
  url = 'https://maps.googleapis.com/maps/api/place/details/json?key=' + google_key + '&placeid=' + place_id
  puts url
  results = {}
  uri = URI.parse(url)
  http = Net::HTTP.new(uri.host, uri.port)
  http.open_timeout = 30
  http.read_timeout = 30
  http.use_ssl = true
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  response = http.request_post("#{uri.path}?#{uri.query}", '')
  results = response.body
  json = JSON.parse(results)
  if json['status'] == 'OK'
    website = json['result']['website'] unless json['result']['website'].nil?
  end
  website = Addressable::URI.parse(website).host
  return website
end

def semrush_responses(urls)
  urls = urls.uniq
  urls.each do |url|
    response = semrush_api_request(url)
    if response.body != "ERROR 50 :: NOTHING FOUND"
      SemrushPaidReport.create(url: url,response: response)
      puts '================================' 
    end
  end
end

def semrush_api_request(url)
  apibase = "http://api.semrush.com/"
  type = "domain_adwords"
  semrush_key = "7be3a20adbd7dd4ea65961b82df5f865"
  display_limit = "10"
  export_columns = "Ph,Po,Pp,Pd,Nq,Cp,Vu,Tr,Tc,Co,Nr,Td"
  #@domain = "ebay.com"
  display_sort = "po_asc"
  database = "us"
  response = HTTParty.get("#{apibase}?type=#{type}&key=#{semrush_key}&display_limit=#{display_limit}&export_columns=#{export_columns}&domain=#{url}&display_sort=#{display_sort}&database=#{database}")
  return response
end