Rails.application.routes.draw do

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  get '/dashboard' => "web_reports#index", as: :reports_dash
  get '/reports/wpscan' => "web_reports#wpscan_report", as: :wpscans_report
  get '/reports/whatweb' => "web_reports#whatweb_report", as: :whatwebs_report
  get '/reports/google_business_malware' => "web_reports#malware_report", as: :google_malware_report
  get '/reports/semrush_report' => "web_reports#semrush_report", as: :web_semrush_report
  get '/set_claim/:id' => "web_reports#set_claim_lead", as: :set_claim
  post '/create_lead_comment' => "leads#add_comment", as: :add_comment
  get '/reports/semrush/:campaign' => "semrush#index"

  #post '/tools/whatweb', to: 'whatweb#tools'

  match '/wizard/step2', to: 'wizard#step2', as: 'step2', via: [:get, :post]
  match '/wizard/step3', to: 'wizard#step3', as: 'step3', via: [:get, :post]
  match '/wizard/step4', to: 'wizard#step4', as: 'step4', via: [:get, :post]

  match '/tools/address2gps', to: 'tools#address2gps', as: 'address2gps', via: [:get, :post]
  match '/tools/placeids', to: 'tools#placeids', as: 'placeids', via: [:get, :post]
  match '/tools/placedetails', to: 'tools#placedetails', as: 'placedetails', via: [:get, :post]

  match '/tools/arachni', to: 'tools#arachni', as: 'arachni', via: [:get, :post]
  match '/tools/skipfish', to: 'tools#skipfish', as: 'skipfish', via: [:get, :post]
  match '/tools/w3af', to: 'tools#w3af', as: 'w3af', via: [:get, :post]
  match '/tools/wapiti', to: 'tools#wapiti', as: 'wapiti', via: [:get, :post]
  match '/tools/whatweb', to: 'tools#whatweb', as: 'whatweb', via: [:get, :post]
  match '/tools/wpscan', to: 'tools#wpscan', as: 'wpscan', via: [:get, :post]

  root to: 'web_reports#index'

  resources :leads
  resources :comments
  

  devise_for :users

  resources :users
  resources :businesses
  resources :place_details, only: :index
  resources :semrush_paid_reports,only: :index
  resources :wizard

  scope module: :api, defaults: {format: :json} do
      namespace :v1 do
        resources :whatwebs,only: :index
        resources :wpscans,only: :index
        resources :joomscans,only: :index
        resources :semrush_paid_reports,only: :index
        resources :arachni,only: :index
        resources :semrush_paid_reports,only: :index

      end
    end
end
