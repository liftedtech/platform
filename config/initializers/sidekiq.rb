Sidekiq.configure_server do |config|
  config.redis = { url: 'redis://redistogo:9a0ee2df8d7b33b06ec4bf4a0581c9a2@crestfish.redistogo.com:9883' }
end

Sidekiq.configure_client do |config|
  config.redis = { url: 'redis://redistogo:9a0ee2df8d7b33b06ec4bf4a0581c9a2@crestfish.redistogo.com:9883' }
end
