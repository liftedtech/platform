require 'rails_helper'

describe PlaceDetailsController, type: :controller do
  include Devise::TestHelpers
  before :each do
    @user = FactoryGirl.create(:user)
    sign_in @user
  end

  describe "GET index" do
    before(:each) do
      (1..20).each do |i|
        FactoryGirl.create(:place_detail, name: "place_detail-#{i}")
      end
    end
    
    it "has a 200 status code" do
      get :index
      expect(response.status).to eq(200)
    end
    
    it "show created place details equal to 20" do
      get :index
      params = {"controller"=>"place_details", "action"=>"index"}
      expect(PlaceDetail.search(params[:q]).result.count).to eq 20
    end
    
  end
  
end
