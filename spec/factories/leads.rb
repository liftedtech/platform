FactoryGirl.define do
  factory :lead do
    url "MyString"
    formatted_address "MyString"
    phone_number "MyString"
    vulnerability_count 1
  end
end
