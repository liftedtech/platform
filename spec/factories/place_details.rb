# == Schema Information
#
# Table name: place_details
#
#  id                         :integer          not null, primary key
#  place_id                   :integer
#  name                       :string
#  google_id                  :string
#  google_place_id            :string
#  url                        :string
#  utc_offset                 :string
#  vicinity                   :string
#  website                    :string
#  international_phone_number :string
#  raw                        :text
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

FactoryGirl.define do
  factory :place_detail do
    name 'MyString'
    google_id 'MyString'
    place_id 'MyString'
    url 'MyString'
    utc_offset 'MyString'
    vicinity 'MyString'
    website 'MyString'
    international_phone_number 'MyString'
    raw 'MyText'
  end
end
