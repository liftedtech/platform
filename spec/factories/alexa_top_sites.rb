# == Schema Information
#
# Table name: alexa_top_sites
#
#  id          :integer          not null, primary key
#  rank        :integer
#  domain_name :string
#  date        :date
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :alexa_top_site do
    rank 1
    domain_name 'MyString'
    date '2016-10-31'
  end
end
