require 'spec_helper'
require 'rake'

describe 'scrape_data:safe_browsing_api' do
  before { RailsDevise::Application.load_tasks }
  before :each do
    ["http://kingskillz.ru/","http://coffeol.com/"].each do |url|
      FactoryGirl.create(:place_detail,url: url)
    end
  end
  it { expect { Rake::Task['scrape_data:safe_browsing_api'].invoke }.not_to raise_exception }
end