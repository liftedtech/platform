class ScrapeEmailService
  def scrape(target)

    # w3af can take a very long time to complete, so we background it and send a sidekiq pid as confirmation

    return if Wpscan.where(url: url).exists? == true
    print "[INFO]".colorize[:blue] + " E-mail scrape initializing for " + "#{url}".colorize[:green]
    result = `platform/skipfish -u "#{url}" --no-color --batch -r --follow-redirection'`
    print "[SUCCESS]".colorize[:blue] + " API queried to queue up scrape"
    Wpscan.create(url: url, response: result)

  end
end
