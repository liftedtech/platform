class WpscanService
  def scan(url, tag)

    return if Wpscan.where(url: url).exists? == true
    print "[!] TOOL: wpscan | TARGET: #{url}"
    result = `wpscan -u "#{url}" --no-color --batch -r --follow-redirection'`
    print "[$] WPScan Results Complete."
    vulns = 0
    if result =~ /(\d+) vulnerabilities identified from the version number/
     vulns = $1
    end
    Wpscan.create(url: url, response: result, tag: tag, vulnerability_count: vulns)

  end
end
