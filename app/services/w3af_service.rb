class W3afService
  def scan(url)

    # w3af can take a very long time to complete, so we background it and send a sidekiq pid as confirmation

    return if W3af.where(url: url).exists? == true
    print "[!] TOOL: w3af | TARGET: #{url}"
    result = `platform/w3af -u "#{url}" --no-color --batch -r --follow-redirection'`
    print "[$] w3af Scan Complete."
    W3af.create(url: url, response: result)

  end
end
