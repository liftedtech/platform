class PlaceIdsService
  
  def getch_places(lat,lng)
    websites = []
    keywords = []
    path = Rails.root.join('lib/tasks', 'keywords1.txt')
    f = File.open(path)
    records = f.readlines
    puts 'pulling keywords'
    records.each do |record|
      keywords << record
    end
    puts keywords

    puts 'pulling places'
    google_key = 'AIzaSyADZOp2jbnLd0UTXERo5xDMeLJp45XKLk4'
    keywords.each do |keyword|
      puts keyword
      keyword = keyword.strip
      url = 'https://maps.googleapis.com/maps/api/place/radarsearch/json?key=' + google_key + '&location=' + lat.to_s + ',' + lng.to_s + '&keyword=' + keyword + '&radius=50000'
      puts url
      results = {}
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.open_timeout = 30
      http.read_timeout = 30
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      response = http.request_post("#{uri.path}?#{uri.query}", '')
      if Net::HTTPOK # 200
        results = response.body
        json = JSON.parse(results)
        if json['status'] == 'OK'
          place_ids = json['results'].collect{|r| r['place_id']} 
          place_ids.each do |place_id|
            websites << get_place_id_details(place_id)
          end
        end
      end
      puts '********************************************'
    end
    return websites
  end
  
  def get_place_id_details(place_id)
    google_key = 'AIzaSyADZOp2jbnLd0UTXERo5xDMeLJp45XKLk4'
    url = 'https://maps.googleapis.com/maps/api/place/details/json?key=' + google_key + '&placeid=' + place_id
    puts url
    results = {}
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.open_timeout = 30
    http.read_timeout = 30
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    response = http.request_post("#{uri.path}?#{uri.query}", '')
    results = response.body
    json = JSON.parse(results)
    if json['status'] == 'OK'
      website = json['result']['website'] unless json['result']['website'].nil?
    end
    website = Addressable::URI.parse(website).host
    return website
  end  
  
end