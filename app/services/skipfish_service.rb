class SkipfishService
  def scan(url)

    # w3af can take a very long time to complete, so we background it and send a sidekiq pid as confirmation

    return if Skipfish.where(url: url).exists? == true
    print "[!] TOOL: skipfish | TARGET: #{url}"
    result = `platform/skipfish -u "#{url}" --no-color --batch -r --follow-redirection'`
    print "[$] Skipfish Scan Complete."
    Skipfish.create(url: url, response: result)

  end
end
