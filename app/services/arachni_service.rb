class ArachniService
  def scan(url)

    # wapiti can take a very long time to complete, so we background it and send a sidekiq pid as confirmation

    return if Arachni.where(url: url).exists? == true
    print "[!] TOOL: arachni | TARGET: #{url}"
    result = `platform/wapiti -u "#{url}" --no-color --batch -r --follow-redirection'`
    print "[$] Arachni Scan Complete."
    Wpscan.create(url: url, response: result)

  end
end
