class WhatwebService
  def scan(url, tag)

    return if Whatweb.where(url: url).exists? == true

    print "[!] TOOL: whatweb | TARGET: #{url}"
    result = `/usr/bin/whatweb --color=never -a2 #{url}`

    if result.include? "WordPress"
      WpscanWorker.perform_async(url, tag)
    end

    print "Result: #{result} --"
    Whatweb.create(url: url, response: result.gsub(/\\/,'|').encode('utf-8'), tag: tag)

  end
end
