class JoomscanService
  def scan(url)

    return if Joomscan.where(url: url).exists? == true
    result = `joomscan -u "#{url}"`
    Joomscan.create(url: url, response: result)
    print "[SUCCESS]".colorize=[:green] + " Response received and saved. "

  end
end
