class WapitiService
  def scan(url)

    # wapiti can take a very long time to complete, so we background it and send a sidekiq pid as confirmation

    return if Wapiti.where(url: url).exists? == true
    print "[!] TOOL: wpscan | TARGET: #{url}"
    result = `platform/wapiti -u "#{url}" --no-color --batch -r --follow-redirection'`
    print "[$] Wapiti Scan Complete."
    Wapiti.create(url: url, response: result)

  end
end
