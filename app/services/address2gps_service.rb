class Address2gpsService
  def convert(address)

    require 'colorize'
    require 'httparty'

    apikey = GOOGLE_GEOCODE_KEY
    address.gsub!(" ","%20")
    url = 'https://maps.googleapis.com/maps/api/geocode/json' + '?key=' + apikey + '&address=' + address

    puts "[INFO]".colorize(:yellow) + "Geocoding #{address} via #{url}"

    response = HTTParty.get(url)
    json = response.parsed_response #    json = JSON.parse(results)

    lat = json['results'][0]['geometry']['location']['lat']
    lng = json['results'][0]['geometry']['location']['lng']

    print "[SUCCESS]".colorize(:green) + " Latitude: #{lat} | Longitude: #{lng}"

    { lat: lat, lng: lng }

  end
end
