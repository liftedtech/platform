class GooglePlacesRadarSearchService
  def radarsearch(lat, lng, tag)
    require 'httparty'
    require 'colorize'
    require 'awesome_print'
    require 'fileutils'
    require 'awesome_print'

    key = GOOGLE_RADAR_SEARCH_KEY
    lat ||= "35.5951"
    lng ||= "-82.5515"
    tag ||= "dentist"

    File.readlines('lib/tasks/keywords1.txt').each do |businesstype|

      url = "https://maps.googleapis.com/maps/api/place/radarsearch/json?key=#{key}&location=#{lat},#{lng}&keyword=#{businesstype}&radius=50000"

      response = HTTParty.get(url)
      json = response.parsed_response

      json['results'].each do |place|
        print "[SUCCESS] Captured #{place['place_id']} from category(#{businesstype}). Grabbing details now..."
        GooglePlacesGetDetailsService.new.placesdetails(place['place_id'], tag)
      end

    end
  end
end
