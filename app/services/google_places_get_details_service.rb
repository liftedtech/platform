class GooglePlacesGetDetailsService
  def placesdetails(place_id, tag)
    require 'colorize'
    require 'httparty'
    require 'awesome_print'
    apikey = GOOGLE_PLACEDETAIL_KEY
    url = 'https://maps.googleapis.com/maps/api/place/details/json' + '?key=' + apikey + '&placeid=' + place_id.to_s

    return if PlaceDetail.where(google_place_id: place_id).exists? == true

    puts "[INFO]".colorize(:yellow) + "Requesting details for Google Place ID #{place_id}..."

    response = HTTParty.get(url) rescue nil
    json = response.parsed_response rescue nil

    begin

      PlaceDetail.create(name: json['result']['name'],
                        google_id: json['result']['id'],
                        google_place_id: json['result']['place_id'],
                        url: json['result']['url'],
                        utc_offset: json['result']['utc_offset'],
                        vicinity: json['result']['vicinity'],
                        website: json['result']['website'],
                        international_phone_number:json['result']['international_phone_number'] ,
                        lng: json['result']['geometry']['location']['lng'],
                        lat: json['result']['geometry']['location']['lat'],
                        formatted_address: json['result']['formatted_address'],
                        raw: json['result'],
                        tag: tag)

      WhatwebWorker.perform_async(json['result']['website'], tag)

    rescue Exception => error
                        print "[ERROR] /app/services/google_places_get_details_service.rb: #{error.message}"
    end

  end
end
