class GenericFingerprint
  def scan(target)

    # generic template for searching our Website Address Inventory
    # when new website vulnerabilities get discovered/published
    # we load in new "finerprints" and regularly check all websites
    # for example: existence of files, regex statements in the HTML, etc.

    print "[!] TOOL: Fingerprint ABCXYZ | TARGET: #{target}"

  end
end
