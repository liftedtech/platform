# == Schema Information
#
# Table name: alexa_top_sites
#
#  id          :integer          not null, primary key
#  rank        :integer
#  domain_name :string
#  date        :date
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class AlexaTopSite < ActiveRecord::Base
  validates :domain_name, uniqueness: true
end
