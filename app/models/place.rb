# == Schema Information
#
# Table name: places
#
#  id         :integer          not null, primary key
#  place_id   :string
#  lat        :float
#  lng        :float
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Place < ActiveRecord::Base
  validates :place_id, uniqueness: true
  has_many :place_details
  belongs_to :keyword
end
