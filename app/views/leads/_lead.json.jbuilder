json.extract! lead, :id, :url, :formatted_address, :phone_number, :,, :vulnerability_count, :created_at, :updated_at
json.url lead_url(lead, format: :json)