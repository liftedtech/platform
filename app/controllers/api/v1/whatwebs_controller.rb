class Api::V1::WhatwebsController < ApiController

  def index
    result = `/usr/bin/whatweb --color=never -a2 #{canonical(params[:url])}`

    if result.include? "WordPress"
      WpscanWorker.perform_async(params[:url]) # auto-audit wordpress
    end

    render json: { result: result }
  end

end
