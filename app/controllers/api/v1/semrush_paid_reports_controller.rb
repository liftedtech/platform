class Api::V1::SemrushPaidReportsController < ApiController

  # http://api.semrush.com/
  # ?type=domain_adwords
  # &key=7be3a20adbd7dd4ea65961b82df5f865
  # &display_limit=10
  # &export_columns=Ph,Po,Pp,Pd,Nq,Cp,Vu,Tr,Tc,Co,Nr,Td
  # &domain=ebay.com
  # &display_sort=po_asc
  # &database=us

  

  def index
    @apibase = "http://api.semrush.com/"
    @type = "domain_adwords"
    @semrush_key = "7be3a20adbd7dd4ea65961b82df5f865"
    @display_limit = "10"
    @export_columns = "Ph,Po,Pp,Pd,Nq,Cp,Vu,Tr,Tc,Co,Nr,Td"
    #@domain = "ebay.com"
    @display_sort = "po_asc"
    @database = "us"
    response = HTTParty.get("#{@apibase}?type=#{@type}&key=#{@semrush_key}&display_limit=#{@display_limit}&export_columns=#{@export_columns}&domain=#{canonical(params[:url])}&display_sort=#{@display_sort}&database=#{@database}")
    render json: { result: response }
  end

end


