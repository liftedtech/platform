class Api::V1::WpscansController < ApiController

  def index
    # we want this to simply give us an ID number, and when this shell script completes, it will update
    # result = `sudo su - liftedtechnology -c '/home/liftedtechnology/security/wpscan/wpscan.rb -u "#{params[:url]}" --no-color --batch -r --follow-redirection -e u'`
    # check if URL was scanned into Wpscans already before scanning it
    sidekiqID = WpscanWorker.perform_async(params[:url])
  #  result = `/home/liftedtechnology/security/wpscan-worker.sh -u "#{params[:url]}"`
    render json: { sidekiqID: sidekiqID, url: params[:url] }
  end

end
