class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  #before_filter :authenticate_user!
  skip_before_filter  :verify_authenticity_token
  before_action :configure_permitted_parameters, if: :devise_controller?
  protected

  # defining our API endpoints for rotating sidekiq queues

  api_endpoints = ["amazon1.secureli.com", "amazon2.secureli.com", "amazon3.secureli.com",
  "amazon4.secureli.com", "amazon5.secureli.com", "amazon6.secureli.com", "amazon7.secureli.com",
"amazon8.secureli.com", "google2.secureli.com", "google3.secureli.com", "google4.secureli.com",
"google5.secureli.com", "amazon6.secureli.com", "microsoft1.secureli.com", "microsoft2.secureli.com",
 "microsoft3.secureli.com", "microsoft4.secureli.com" ]

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_in) << :username
    devise_parameter_sanitizer.for(:sign_up) << :username
  end
  
  def find_lat_lngs_array
    @locations = []
    @whatwebs.each do |whatweb|
      place_detail = PlaceDetail.find_by_website whatweb.url
      unless place_detail.nil?
        begin
          puts '***********************' , place_detail.website.inspect
          @locations << [place_detail.lat.to_f,place_detail.lng.to_f] unless place_detail.lat.nil? and place_detail.lng.nil?
          #@coordinates = Address2gpsService.new.convert(place_detail.formatted_address.gsub(" ","%20"))
          #@locations << [@coordinates[:lat],@coordinates[:lng]]
        rescue => e
          puts '****************** error message' , e.message 
        end
      end
    end
  end
  
  def find_lat_lngs_array_for_wpscans
    @locations = []
    @wpscans.each do |wpscan|
      place_detail = PlaceDetail.find_by_website wpscan.url
      unless place_detail.nil?
        begin
          puts '***********************' , place_detail.website.inspect
          @locations << [place_detail.lat.to_f,place_detail.lng.to_f] unless place_detail.lat.nil? and place_detail.lng.nil?
          #@coordinates = Address2gpsService.new.convert(place_detail.formatted_address.gsub(" ","%20"))
          #@locations << [@coordinates[:lat],@coordinates[:lng]]
        rescue => e
          puts '****************** error message' , e.message 
        end
      end
    end
  end
  
end
