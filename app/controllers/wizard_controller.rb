class WizardController < ApplicationController
  def index
    @status = "new"
  end
  def step2
      if params[:address].present?
        session[:campaignName] = params[:campaignName]
        @address = params[:address]
        @coordinates = Address2gpsService.new.convert(@address)
        @tag = params[:campaignName]
        GooglePlacesWorker.perform_async(@coordinates[:lat],@coordinates[:lng], @tag)
        @status = "step2"
        @path = step2_path
        #@campaignplaces = PlaceDetail.where(tag: @tag).page(params[:page])
        #@locations = @campaignplaces.collect{|c| [c.lat.to_f,c.lng.to_f]}
      else
        @campaignplaces = PlaceDetail.where(tag: session[:campaignName]).page(params[:page]).per(25)
        @locations = @campaignplaces.collect{|c| [c.lat.to_f,c.lng.to_f]}
      end


    respond_to do |format|
      format.js
      format.html
    end
  end

  def step3
    @status = "step3"
    @search = Whatweb.where(campaign_id: session[:campaignName]).search(params[:q])
    @whatwebs = @search.result.page(params[:page]).per(25)
    find_lat_lngs_array
    @path = step3_path
    respond_to do |format|
      format.js
      format.html
    end
  end

  def step4
    @status = "step4"
    @search = Wpscan.where(campaign: session[:campaignName]).search(params[:q])
    @wpscans = @search.result.page(params[:page]).per(25)
    find_lat_lngs_array_for_wpscans
    @path = step4_path
    respond_to do |format|
      format.js
      format.html
    end
  end

end
