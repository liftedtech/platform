class WebReportsController < ApplicationController
  before_action :authenticate_user!

  def index
    if params[:campaign_id].present? and !params[:campaign_id].nil?
      # @semrush_report = SemrushPaidReport.where(campaign_id: params[:campaign_id])
      @gbusiness_malware = GoogleBusinessMalware.where(campaign_id: params[:campaign_id])
      @wpscans = Wpscan.where(campaign: params[:campaign_id])
      @whatwebs = Whatweb.where(campaign_id: params[:campaign_id])
    else
      # @semrush_report = SemrushPaidReport.all
      @gbusiness_malware = GoogleBusinessMalware.all
      @wpscans = Wpscan.all
      @whatwebs = Whatweb.all
    end
    @options = {"Whatweb"=>PlaceDetail.count/Whatweb.count, "Wpscan"=>PlaceDetail.count/Wpscan.count, "Semrush"=>PlaceDetail.count/SemrushPaidReport.count}
  end

  def semrush_report
    @search = SemrushPaidReport.search(params[:q])
    @semrush_report = @search.result.page(params[:page]).per(10)
    @path = web_semrush_report_path

  	respond_to do |format|
      format.js
      format.html
    end
  end
  def malware_report
    @search = GoogleBusinessMalware.search(params[:q])
    @gbusiness_malware = @search.result.page(params[:page]).per(10)
    @path = google_malware_report_path

  	respond_to do |format|
      format.js
      format.html
    end
  end
  def wpscan_report
    @search = Wpscan.order(:vulnerability_count => 'desc').search(params[:q])
    @wpscans = @search.result.page(params[:page]).per(10)
    @wpscans = @wpscans.where("response like ?", "%vulnerabilities identified from the version number%")
  	@path = wpscans_report_path
  	respond_to do |format|
      format.js
      format.html
    end
  end

  def whatweb_report
    @search = Whatweb.search(params[:q])
    @whatwebs = @search.result.page(params[:page]).per(10)
    find_lat_lngs_array
    @path = whatwebs_report_path
    respond_to do |format|
      format.js
      format.html
    end
  end

  def set_claim_lead
    @message = "Lead can't be claimed"
    wpscan = Wpscan.find_by_id(params[:id])
    
    @place = PlaceDetail.where(website: wpscan.url).first
    @alexa = AlexaTopSite.where(domain_name: wpscan.url)
    
    
    wpscan.update_attributes(claimed: true)
    
    @lead = Lead.new()
    if @place.present?
      @lead.url = wpscan.url ||= ""
      @lead.formatted_address = @place.formatted_address ||= "" 
      @lead.phone_number = @place.international_phone_number ||= "" 
      @lead.vulnerability_count = wpscan.vulnerability_count ||= ""
    end
    if @lead.present? and @lead.save!
      @message = "Lead claimed successfully"
    end
    flash[:notice] = @message
    redirect_to :back
  end
  
end
