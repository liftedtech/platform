class ToolsController < ApplicationController
  def tools
   result = WhatwebWorker.new.perform(params[:target_url])
   render json: { result: result }
 end
  def index
    @status = "new"
  end
  def address2gps
    @status = "address2gps"
    result = Address2gpsService.new.perform(params[:address])
    render json: { result: result }
  end
  def arachni
    @status = "step3"
  end
  def joomscan
    @status = "step4"
  end
  def placedetails
    @status = "step4"
  end
  def placeids
    @status = "step4"
  end
  def skipfish
    @status = "step4"
  end
  def w3af
    @status = "step4"
  end
  def wapiti
    @status = "step4"
  end
  def whatweb
    @status = "step4"
  end
  def wpscan
    @status = "step4"
  end
end
