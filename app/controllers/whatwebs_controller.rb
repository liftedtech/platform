class WhatwebsController < ApplicationController
  def index
    @search = Whatweb.search(params[:q])
    @whatwebs = @search.result.page(params[:page]).per(5)
    respond_to do |format|
      format.js
      format.html
    end
  end
  def tools
   result = WhatwebWorker.new.perform(params[:target_url])
   render json: { result: result }
 end
end
