class SemrushPaidReportsController < ApplicationController
  def index
    @search = SemrushPaidReport.search(params[:q])
    @semrush_paid_reports = @search.result.page(params[:page])
  end
end