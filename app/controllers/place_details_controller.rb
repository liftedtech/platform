class PlaceDetailsController < ApplicationController
  def index
    @search = PlaceDetail.search(params[:q])
    @place_details = @search.result.page(params[:page])
  end
end