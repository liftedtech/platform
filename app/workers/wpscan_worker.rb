class WpscanWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :wpscantwo
  require 'benchmark'

  def perform(targetURL, tag)
      begin
        return if Wpscan.where(url: targetURL).exists? == true
        print "[!] TOOL: wpscan | TARGET: #{targetURL}"
        result = `sudo su - root -c '/tools/wpscan/wpscan.rb -u "#{targetURL}" --no-color --batch --follow-redirection'`
        print "[$] WPScan Results Complete: #{result}"
        Wpscan.create(url: targetURL, response: result, tag: tag)
            rescue => e
        puts '================= error', e.message
      end

  end
end
