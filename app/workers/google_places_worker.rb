class GooglePlacesWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :default
  require 'benchmark'

  def perform(lat, lng, tag) #three correct?
    GooglePlacesRadarSearchService.new.radarsearch(lat, lng, tag)
  end
end
