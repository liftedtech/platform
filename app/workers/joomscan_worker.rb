class JoomscanWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :joomscan
  require 'benchmark'

  def perform(targetURL)
    begin

      return if Joomscan.where(url: url).exists? == true
      result = `joomscan -u "#{url}"`
      Joomscan.create(url: url, response: result)
      print "[SUCCESS]".colorize=[:green] + " Response received and saved. "

    rescue => e
          puts '================= error', e.message
    end

  end
end
