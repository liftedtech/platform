class AlexaTopSitesSafeBrowsingWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :safe_browsing
  require 'benchmark'

  def perform(index)
    begin
      key = GOOGLE_SAFEBROWSING_KEY
      url = "https://safebrowsing.googleapis.com/v4/threatMatches:find?key=#{key}"
      thread_entries = []
      alexa_top_sites = AlexaTopSite.page(index).per(500)
      alexa_domains = alexa_top_sites.map(&:domain_name)
      alexa_domains.each do |domain|
        thread_entries << {:url=>domain}
      end
      response = HTTParty.post(url,
        :body => {:client=>{:clientId=>'opslog', :clientVersion=>'1.0.0'},
          :threatInfo=>{:platformTypes=>['ANY_PLATFORM'],
            :threatEntries=>thread_entries,
            :threatEntryTypes=>['THREAT_ENTRY_TYPE_UNSPECIFIED',
              'URL',
              'EXECUTABLE',
              'IP_RANGE'],
            :threatTypes=>['THREAT_TYPE_UNSPECIFIED',
              'MALWARE',
              'SOCIAL_ENGINEERING',
              'UNWANTED_SOFTWARE',
              'POTENTIALLY_HARMFUL_APPLICATION']}}.to_json,
        :headers => { 'Content-Type' => 'application/json' } )
      if response.code == "200"
        puts "clean" , response.body, response.code, response.message, response.headers.inspect
      else
          puts "dirty" , response.body, response.code, response.message, response.headers.inspect
      end
    rescue => e
      puts '[>.<] ERROR: ', e.message
    end

  end
end
