class WhatwebWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :whatweb
  require 'benchmark'

  def perform(targetURL, tag)
      begin
        WhatwebService.new.scan(targetURL, tag)
      rescue => e
        puts '================= error', e.message
      end

  end
end
